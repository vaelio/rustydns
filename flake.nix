# flake.nix. Use by typing 'nix develop' in the main directory of the project
{
  description = "rust devshell for rustydns";

  inputs = {
    fenix.url = "github:nix-community/fenix";
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, fenix, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

	toolchain = fenix.packages.${system}.stable.rust;

	naersk' = naersk.lib.${system}.override {
	  cargo = toolchain;
	  rustc = toolchain;
	};

      in
      {
        packages = {
	  rustydns = naersk'.buildPackage {
	    src = ./.;
	    pname = "rustydns";
	  };
	};
        defaultPackage = self.packages.${system}.rustydns;
        devShells.default = with pkgs; mkShell {
          buildInputs = [
            pkg-config
	    cargo
	    rustc
	    rustfmt
            bacon
          ];
        };
      }
   );
}
