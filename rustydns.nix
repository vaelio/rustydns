{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.rustydns;
in {
  options.services.rustydns = {
    enable = mkEnableOption "rustydns service";
    resolver = mkOption {
      type = types.nullOr types.str;
      default = null;
    };
    port = mkOption {
      type = types.port;
      default = 53;
    };
    threads = mkOption {
      type = types.nullOr types.ints.u8;
      default = null;
    };
    custom_hosts_file = mkOption {
      type = types.nullOr types.path;
      default = null;
    };
    bind_addr = mkOption {
      type = types.nullOr types.str;
      default = null;
    };
    ttl_offset = mkOption {
      type = types.nullOr types.ints.u32;
      default = null;
    };
    udp.enable = mkEnableOption "Enable UDP socket bind"
                 // { default = true; };
    tcp.enable = mkEnableOption "Enable TCP socket bind";
  };

  
  config = mkIf cfg.enable {
    systemd.services.rustydns = {
      wantedBy = [ "multi-user.target" ];
      serviceConfig.ExecStart = ''${pkgs.nix}/bin/nix run --refresh https://gitlab.com/vaelio/rustydns/-/archive/main/rustydns-main.zip --'' 
                                + (optionalString (cfg.resolver != null) " --custom-dns-resolver ${cfg.resolver}") 
                                + " -p" + (toString cfg.port) 
                                + (optionalString (cfg.custom_hosts_file != null) " --hosts ${cfg.custom_hosts_file}") 
                                + (optionalString cfg.tcp.enable " -t") 
                                + (optionalString cfg.udp.enable " -u") 
                                + (optionalString (cfg.bind_addr != null) " -a ${cfg.bind_addr}") 
                                + (optionalString (cfg.threads != null) (" -n " + (toString cfg.threads)))
                                + (optionalString (cfg.ttl_offset != null) (" -T " + (toString cfg.ttl_offset)))
                                + " -v" ;
    };
  };

}
