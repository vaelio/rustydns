use clap::Parser;
use tokio::runtime::Builder;

use nom::{
    IResult,
};
use mini_moka::sync::Cache;
use std::time::Duration;
use std::thread::sleep;

use crate::udpserver::UdpServer;
use crate::tcpserver::TcpServer;
use crate::server::Server;
use crate::hosts_file_parser::Host;

mod dnslabel;
mod dnsrecord;
mod dnsmessage;
mod dnsquestion;
mod udpserver;
mod tcpserver;
mod server;
mod hosts_file_parser;

type Stderr = Box<dyn std::error::Error + Sync + Send>;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Config {
    #[arg(short, long, default_value = "127.0.0.1")]
    address: String,

    #[arg(short, long, default_value_t = 1053)]
    port: usize,

    #[arg(short, long, default_value_t = false)]
    tcp: bool,

    #[arg(short, long, default_value_t = true)]
    udp: bool,

    #[arg(short='T', long, default_value_t = 0)]
    ttl_offset: u64,

    #[arg(long, default_value = None)]
    custom_dns_resolver: Option<String>,

    #[arg(short='n', long, default_value_t = 4)]
    thread_count: usize,

    #[arg(short='v', long, default_value_t = false)]
    verbose: bool,

    #[arg(long="hosts", default_value = None)]
    custom_hosts_file: Option<String>,

}




#[tokio::main]
async fn main() {
    let cfg = Config::parse();

    dbg!(&cfg);

    let hosts = if let Some(path) = cfg.custom_hosts_file {
        Some(Host::from_file(&path).expect("There was a problem with your file format"))
    } else {
        None
    };

    let cache = Cache::builder()
        .time_to_live(Duration::from_secs(30*60))
        .time_to_idle(Duration::from_secs(5*60))
        .max_capacity(10_000)
        .build();
    

    let tcp_runtime = Builder::new_multi_thread()
        .worker_threads(cfg.thread_count)
        .thread_name("dns-tcpserver-runtime")
        .enable_io()
        .build()
        .unwrap();

    let udp_runtime = Builder::new_multi_thread()
        .worker_threads(cfg.thread_count)
        .thread_name("dns-udpserver-runtime")
        .enable_io()
        .build()
        .unwrap();

    let tcp_handle = if cfg.tcp {
        if let Ok(mut tcp_server) = TcpServer::new(&cfg.address, cfg.port, cfg.ttl_offset, cfg.custom_dns_resolver.clone(), cfg.verbose, hosts.clone()).await {
            let cache = cache.clone();
            Some(tcp_runtime.spawn(async move {
                loop {
                    if let Err(e) = tcp_server.run_loop(&cache).await {
                        println!("error: {}", e);
                    }
                }
            }))
            
        } else {
            None
        }
    } else {
        None
    };
    
    let udp_handle = if cfg.udp {
        if let Ok(mut udp_server) = UdpServer::new(&cfg.address, cfg.port, cfg.ttl_offset, cfg.custom_dns_resolver, cfg.verbose, hosts.clone()).await {
            
            let cache = cache.clone();
            Some(udp_runtime.spawn(async move {
                loop {
                    if let Err(e) = udp_server.run_loop(&cache).await {
                        println!("error: {}", e);
                    }
                }
            }))
        } else {
            None
        }
    } else {
        None
    };
    
    let one_sec = Duration::from_millis(1000);
    loop {
        let mut finished = true;
        if let Some(ref handle) = tcp_handle {
            finished = handle.is_finished();
        }

        if let Some(ref handle) = udp_handle {
            finished = handle.is_finished();
        }

        if finished {
            break;
        }

        sleep(one_sec);
    }

}
