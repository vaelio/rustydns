use crate::dnsquestion::DnsQuestion;
use crate::dnsrecord::DnsRecord;
use crate::Stderr;

use nom::IResult;
use nom::bytes::complete::take;
use nom::multi::count;
use byteorder::{ByteOrder, BigEndian};

#[derive(Debug, Clone)]
pub struct DnsMessage {
    id: u16,
    flags: u16,
    n_questions: u16,
    n_answers: u16,
    n_authorities: u16,
    n_additionals: u16,
    raw: Vec<u8>,

    questions: Vec<DnsQuestion>,
    responses: Vec<DnsRecord>,
    authorities: Vec<DnsRecord>,
    additionals: Vec<DnsRecord>
}

impl DnsMessage {

    pub fn new(id: u16, flags: u16, questions: Vec<DnsQuestion>, responses: Vec<DnsRecord>, authorities: Vec<DnsRecord>, additionals: Vec<DnsRecord>) -> Self {
        Self {
            id,
            flags,
            n_questions: questions.len() as u16,
            n_answers: responses.len() as u16,
            n_authorities: authorities.len() as u16,
            n_additionals: additionals.len() as u16,
            raw: vec![],
            questions,
            responses,
            authorities,
            additionals,
        }
    }

    pub fn authorities(&self) -> &[DnsRecord] {
        &self.authorities
    }

    pub fn additionals(&self) -> &[DnsRecord] {
        &self.additionals
    }

    pub fn questions(&self) -> &[DnsQuestion] {
        &self.questions
    }

    pub fn responses(&self) -> &[DnsRecord] {
        &self.responses
    }

    pub fn flags(&self) -> u16 {
        self.flags
    } 

    fn nom_dns_headers(input: &[u8], raw: Vec<u8>) -> IResult<&[u8], Self> {
        let (input, rid) = take(2u8)(input)?;
        let (input, rflags) = take(2u8)(input)?;
        let (input, rn_questions) = take(2u8)(input)?;
        let (input, rn_answers) = take(2u8)(input)?;
        let (input, rn_authorities) = take(2u8)(input)?;
        let (input, rn_additionals) = take(2u8)(input)?;

        let id = BigEndian::read_u16(rid);
        let flags = BigEndian::read_u16(rflags);
        let n_questions = BigEndian::read_u16(rn_questions);
        let n_answers = BigEndian::read_u16(rn_answers);
        let n_authorities = BigEndian::read_u16(rn_authorities);
        let n_additionals = BigEndian::read_u16(rn_additionals);
        let questions = vec![];
        let responses = vec![];
        let authorities = vec![];
        let additionals = vec![];

        let res = Self {
            id,
            flags,
            n_questions,
            n_answers,
            n_authorities,
            n_additionals,
            raw,
            questions,
            responses,
            authorities,
            additionals
        };

        Ok((input, res))
    }

    fn nom_dns_questions(input: &[u8], n_questions: u16) -> IResult<&[u8], Vec<DnsQuestion>> {
        count(DnsQuestion::nom_dns_question, n_questions as usize)(input)
    }

    fn nom_dns_responses(input: &[u8], n_responses: u16) -> IResult<&[u8], Vec<DnsRecord>> {
        count(DnsRecord::nom_dns_resps, n_responses as usize)(input)
    }

    fn nom_dns_additionals(input: &[u8], n_adds: u16) -> IResult<&[u8], Vec<DnsRecord>> {
        count(DnsRecord::nom_dns_adds, n_adds as usize)(input)
    }

    fn nom_dns_authorities(input: &[u8], n_auths: u16) -> IResult<&[u8], Vec<DnsRecord>> {
        count(DnsRecord::nom_dns_auths, n_auths as usize)(input)
    }


    pub fn from_bytes(data: &[u8]) -> Result<Self, Stderr> {
        let raw = data.to_vec();
        if let Ok((mut data, mut msg)) = Self::nom_dns_headers(data, raw.clone()) {
            data = if let Ok((input, mut questions)) = Self::nom_dns_questions(data, msg.n_questions) {
                msg.questions.append(&mut questions);
                input
            } else {
                data
            };
            data = if let Ok((input, mut records)) = Self::nom_dns_responses(data, msg.n_answers) {

                for rec in records.iter_mut() {
                    if rec.compression() {
                        let offset = rec.offset() as usize;
                        rec.set_name(&raw[offset..]);
                    }
                }
                /* After this point, records are uncompressed */
                msg.responses.append(&mut records);
                input
            } else {
                data
            };

            data = if let Ok((data, mut adds)) = Self::nom_dns_additionals(data, msg.n_additionals) {
                msg.additionals.append(&mut adds);
                data
            } else {
                data
            };

            let _ = if let Ok((data, mut auths)) = Self::nom_dns_authorities(data, msg.n_authorities) {
                msg.authorities.append(&mut auths);
                data
            } else {
                data
            };

            return Ok(msg);
        }

        Err(format!("Could not parse msg: {:?}", data).into())
    }

    pub async fn as_bytes(&self, tcp: bool) -> Vec<u8> {
        let headers = self.format_headers_for_response().await;
        let questions = self.format_questions().await;
        let answers = self.format_responses().await;
        let authorities = self.format_authorities().await;
        let additionals = self.format_additionals().await;
        let len = (headers.len() + questions.len() + answers.len() + authorities.len() + additionals.len()) as u16;

        if tcp {
            [&len.to_be_bytes(), &headers[..], &questions, &answers, &authorities, &additionals].concat()
        } else {
            [&headers[..], &questions, &answers, &authorities, &additionals].concat()
        }
    }

    pub async fn format_headers_for_response(&self) -> Vec<u8> {
        let mut res = vec![];

        res.extend_from_slice(&self.id.to_be_bytes());
        res.extend_from_slice(&(0x8180_u16).to_be_bytes());
        res.extend_from_slice(&(self.questions.len() as u16).to_be_bytes());
        res.extend_from_slice(&(self.responses.len() as u16).to_be_bytes());
        res.extend_from_slice(&(self.authorities.len() as u16).to_be_bytes());
        res.extend_from_slice(&(self.additionals.len() as u16).to_be_bytes());

        res
    }

    pub async fn format_questions(&self) -> Vec<u8> {
        let mut res = vec![];

        for q in &self.questions {
            res.append(&mut q.as_bytes());
        }

        res
    }

    pub async fn format_responses(&self) -> Vec<u8> {
        let mut res = vec![];
        for a in &self.responses {
            res.append(&mut a.as_bytes());
        }

        res
    }

    pub async fn format_additionals(&self) -> Vec<u8> {
        let mut res = vec![];
        for additional in &self.additionals {
            res.append(&mut additional.as_bytes());
        }
        res
    }

    pub async fn format_authorities(&self) -> Vec<u8> {
        let mut res = vec![];
        for authority in &self.authorities {
            res.append(&mut authority.as_bytes());
        }
        res
    }

    pub fn raw(&self) -> &[u8] {
        &self.raw
    }

    pub fn id(&self) -> u16 {
        self.id
    }
}