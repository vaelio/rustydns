use crate::dnslabel::DnsNameLabel;
use crate::IResult;
use nom::bytes::complete::take_until;
use nom::bytes::complete::take;
use byteorder::{ByteOrder, BigEndian}; 


#[derive(Debug, Clone)]
pub struct DnsQuestion {
    q_name: Vec<DnsNameLabel>,
    q_type: u16,
    q_class: u16
}

impl DnsQuestion {

    pub fn to_string(&self) -> String {
        format!("{} - {} - {}", self.name_to_string(), self.q_type, self.q_class)
    }

    pub fn name_to_string(&self) -> String {
        let mut strings = vec![];
        for label in &self.q_name {
            strings.push(label.name().to_string());
        } 
        strings.join(".")
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        let mut res = vec![];

        for label in &self.q_name {
            res.extend_from_slice(&label.as_bytes());
        }
        res.push(0);

        res.extend_from_slice(&self.q_type.to_be_bytes());
        res.extend_from_slice(&self.q_class.to_be_bytes());

        res
    }
    
    pub fn nom_dns_question(input: &[u8]) -> IResult<&[u8], DnsQuestion> {
        let (input, rq_name) = take_until("\0")(input)?;
        let (input, _) = take(1u8)(input)?; // remove the \0
        let (input, rq_type) = take(2u8)(input)?;
        let (input, rq_class) = take(2u8)(input)?;

        let (_, q_name) = DnsNameLabel::from_bytes(rq_name)?;
        
        let q_type = BigEndian::read_u16(rq_type);
        let q_class = BigEndian::read_u16(rq_class);

        Ok((input, DnsQuestion {q_name, q_type, q_class}))
    }

    pub fn get_key(&self) -> String {
        format!("{}-{}-{}", self.name_to_string(), self.q_type, self.q_class)
    }
}