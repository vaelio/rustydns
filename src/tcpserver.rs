use async_trait::async_trait;
use tokio::net::TcpListener;
use mini_moka::sync::Cache;
use tokio::io::{
    AsyncWriteExt,
    AsyncReadExt,
};

use crate::Stderr;
use crate::server::Server;
use crate::dnsrecord::DnsRecord;
use crate::hosts_file_parser::Host;

#[derive(Debug)]
pub struct TcpServer {
    listener: TcpListener,
    ttl_offset: u64,
    custom_dns_resolver: Option<String>,
    verbose: bool,
    hosts: Vec<Host>
}

impl TcpServer {
    pub async fn new(addr: &str, port: usize, ttl_offset: u64, custom_dns_resolver: Option<String>, verbose: bool, hosts: Option<Vec<Host>>) -> Result<Self, Stderr> {
        let listener = TcpListener::bind(format!("{}:{}", addr, port)).await?;

        let hosts = if let Some(h) = hosts {
            h
        } else {
            vec![]
        };

        println!("TcpServer bound to: {}:{}", addr, port);
        Ok(Self {listener, ttl_offset, custom_dns_resolver, verbose, hosts})
    }
}

#[async_trait]
impl Server for TcpServer {
    async fn run_loop(&mut self, cache: &Cache<String, Vec<DnsRecord>>) -> Result<(), Stderr> {
        let mut buff = [0; 514];
        loop {
            let (mut client_socket, client_addr) = self.listener.accept().await?;

            let Ok(len) = client_socket.read(&mut buff).await else {
                continue;
            };

            if self.verbose {
                println!("TCP: Received {} bytes from {}", len, client_addr);
            }
            /* 
            when using TCP, DNS message are prefixed by a u16
            representing the message length. We skip those.
            */
            let Ok(dns_query) = Self::parse(&buff[2..len]).await else {
                continue;
            };
            if self.verbose {
                println!("Client {} wants:", client_addr);
                for q in dns_query.questions() {
                    println!("{}", q.to_string())
                }
            }

            let Ok(dns_response) = self.mk_answer(&dns_query, cache, self.ttl_offset, &self.custom_dns_resolver.clone()).await else {
                continue;
            };
            let total = dns_response.as_bytes(true).await;
            
            
            let Ok(_) = client_socket.write_all(&total).await else {
                continue;
            };

        }
    }

    async fn verbose(&self) -> bool {
        self.verbose
    }

    async fn hosts(&self) -> &[Host] {
        &self.hosts
    }
}