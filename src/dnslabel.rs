use nom::IResult;
use nom::bytes::complete::take;
use nom::multi::many0;



#[derive(Debug, Clone, PartialEq)]
pub struct DnsNameLabel {
    size: u8,
    name: String,
}

impl DnsNameLabel {
    pub fn as_bytes(&self) -> Vec<u8> {

        let mut res = vec![];

        res.push(self.size);
        res.extend_from_slice(self.name.as_bytes());

        res
    }

    fn parse_one(input: &[u8]) -> IResult<&[u8], Self> {

        let (input, size) = take(1u8)(input)?;
        let size = size[0];
        let (input, r_name) = take(size)(input)?;

        let name = if size == 0 {
            String::new()
        } else {
            String::from_utf8_lossy(r_name).to_string()
        };

        let res = Self { size, name };
        Ok((input, res))
        
    }

    pub fn from_bytes(input: &[u8]) -> IResult<&[u8], Vec<Self>> {
        many0(Self::parse_one)(input)

    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn from_str(i: &str) -> Vec<Self> {
        let mut res = vec![];
        for s in i.split(".") {
            res.push(
                Self {
                    size: s.len() as u8,
                    name: s.to_string()
                }
            );
        }
        res
    }

}