use std::fs;
use std::io::{self, BufRead};

#[derive(Clone, Debug)]
pub struct Host {
    name: String,
    address: String
}

impl Host {
    pub fn from_file(path: &str) -> Result<Vec<Self>,  Box<dyn std::error::Error>> {
        let mut res = vec![];

        let file = fs::File::open(path)?;
        
        for line in io::BufReader::new(file).lines() {
            let line = line?;
            if !line.starts_with("#") {
                let split = line.split(" ").collect::<Vec<&str>>();

                match (split.get(0), split.get(1)) {
                    (Some(address), Some(name)) => res.push(Self {name: name.to_string(), address: address.to_string()}),
                    _ => println!("Wrong line format: '{}'", line),
                }
            }
        }

        Ok(res)

    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn address_as_bytes(&self) -> Vec<u8> {
        let mut res = vec![];
        for s in self.address.split('.') {
            let number = s.parse::<u8>().unwrap();
            res.push(number);
        }
        res
    }
}