use crate::dnslabel::DnsNameLabel;

use nom::IResult;
use nom::bytes::complete::{take_until, take};
use byteorder::{ByteOrder, BigEndian};
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug, Clone, PartialEq)] 
pub enum DnsRecordType {
    Response,
    Authority,
    Additional,
}

#[derive(Debug, Clone, PartialEq)]
pub struct DnsRecord {
    name: Vec<DnsNameLabel>,
    rtype: u16,
    rclass: u16,
    rttl: i32,
    rdlength: u16,
    rdata: Vec<u8>,
    compression: bool,
    comp_offset: u16,
    record_type: DnsRecordType,
    timestamp: u64,
}

impl DnsRecord {
    
    pub fn new(name: Vec<DnsNameLabel>, rtype: u16, rclass: u16, rttl: i32, rdlength: u16, rdata: Vec<u8>, compression: bool, comp_offset: u16, record_type: DnsRecordType, timestamp: u64) -> Self {
        Self {
            name,
            rtype,
            rclass,
            rttl,
            rdlength,
            rdata,
            compression,
            comp_offset,
            record_type,
            timestamp
        }
    }

    pub fn record_type(&self) -> &DnsRecordType {
        &self.record_type
    }

    pub fn rttl(&self) -> i32 {
        self.rttl
    }

    pub fn ttl_expired(&self, offset: u64) -> bool {
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_secs();
        (now - self.timestamp) > (self.rttl as u64 + offset) && self.rttl != 0
    }

    fn name_as_bytes(&self) -> Vec<u8> {
        let mut res = vec![];
        if !self.name.is_empty() {
            for label in &self.name {
                res.append(&mut label.as_bytes());
            }
        } else {
            res.push(0u8);
        }
        
        res
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        let mut res = vec![];
 
        let offset_or_name = if self.compression {
            (self.comp_offset ^ 0b1100000000000000).to_be_bytes().to_vec()
        } else {
            self.name_as_bytes()
        };
        res.extend_from_slice(&offset_or_name);
        res.extend_from_slice(&self.rtype.to_be_bytes());
        res.extend_from_slice(&self.rclass.to_be_bytes());
        res.extend_from_slice(&self.rttl.to_be_bytes());
        res.extend_from_slice(&self.rdlength.to_be_bytes());
        res.extend_from_slice(&self.rdata);

        res
    }

    pub fn nom_dns_adds(input: &[u8]) -> IResult<&[u8], DnsRecord> {
        Self::nom_dns_record(input, DnsRecordType::Additional)
    }

    pub fn nom_dns_auths(input: &[u8]) -> IResult<&[u8], DnsRecord> {
        Self::nom_dns_record(input, DnsRecordType::Authority)
    }
    
    pub fn nom_dns_resps(input: &[u8]) -> IResult<&[u8], DnsRecord> {
        Self::nom_dns_record(input, DnsRecordType::Response)
    }
    
    pub fn nom_dns_record(input: &[u8], record_type: DnsRecordType) -> IResult<&[u8], DnsRecord> {
        let mut compression = false;
        if input[0] == 0xc0 {
            /* compression activated */
            compression = true;
        }
        let (input, rname) = if compression {
            take(2u8)(input)?
        } else {
            let (input, rname) = take_until("\0")(input)?;
            let input = take(1u8)(input)?.0; // remove trailing \0
            (input, rname)
        };
        let (input, rtype) = take(2u8)(input)?;
        let (input, rclass) = take(2u8)(input)?;
        let (input, rttl) = take(4u8)(input)?;
        let (input, rdlength) = take(2u8)(input)?;

        let rdlength = BigEndian::read_u16(rdlength);

        let (input, rdata) = take(rdlength as usize)(input)?;


        let (_, name) = DnsNameLabel::from_bytes(rname)?;
        
        let rtype = BigEndian::read_u16(rtype);
        let rclass = BigEndian::read_u16(rclass);
        let rttl = BigEndian::read_i32(rttl);

        let timestamp = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_secs();
        if compression {
            let comp_offset = BigEndian::read_u16(rname) & 0b0011111111111111;
            Ok((input, Self {name, rtype, rclass, rttl, rdlength, rdata: rdata.to_vec(), compression, comp_offset, record_type, timestamp}))
        } else {
            Ok((input, Self { name, rtype, rclass, rttl, rdlength, rdata: rdata.to_vec(), compression, comp_offset: 0u16, record_type, timestamp}))
        }
        
    }

    pub fn to_owned(&self) -> Self {
        Self {
            name: self.name.clone(),
            rtype: self.rtype,
            rclass: self.rclass,
            rttl: self.rttl,
            rdlength: self.rdlength,
            rdata: self.rdata.clone(),
            compression: self.compression,
            comp_offset: self.comp_offset,
            record_type: self.record_type.clone(),
            timestamp: self.timestamp
        }
    }

    pub fn set_name(&mut self, bytes: &[u8]) {
        let end = bytes.iter().position(|&r| r == 0).unwrap();
        if let Ok((_, name)) = DnsNameLabel::from_bytes(&bytes[..end]) {
            self.name = name.to_owned();
        }
    }

    pub fn compression(&self) -> bool {
        self.compression
    }

    pub fn offset(&self) -> u16 {
        self.comp_offset
    }

}