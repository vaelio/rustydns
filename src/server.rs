use async_trait::async_trait;
use mini_moka::sync::Cache;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::io::{
    AsyncWriteExt,
    AsyncReadExt,
};

use crate::Stderr;
use crate::dnsmessage::DnsMessage;
use crate::dnslabel::DnsNameLabel;
use crate::dnsrecord::DnsRecord;
use crate::dnsrecord::DnsRecordType;
use crate::hosts_file_parser::Host;

#[async_trait]
pub trait Server {
    
    async fn run_loop(&mut self, cache: &Cache<String, Vec<DnsRecord>>) -> Result<(), Stderr>;

    async fn parse(data: &[u8]) -> Result<DnsMessage, Stderr> {
        let query = DnsMessage::from_bytes(data)?;
        Ok(query)
    }

    async fn verbose(&self) -> bool;
    async fn hosts(&self) -> &[Host];
    
    async fn mk_answer(&mut self, q: &DnsMessage, c: &Cache<String, Vec<DnsRecord>>, ttl_offset: u64, custom_dns_resolver: &Option<String>) -> Result<DnsMessage, Stderr> {
        let mut answers = vec![];
        let mut authorities = vec![];
        let mut additionals = vec![];
        let id = q.id();
        let flags = q.flags();

        for query in q.questions() {
            let key = query.get_key();
            let mut expired = false;
            let mut incache = false;
            let mut inhosts = false;
            
            if let Some(res) = self.hosts().await.iter().find(|x| x.name() == query.name_to_string()) {
                inhosts = true;
                if self.verbose().await {
                    println!("Found '{:?}' in custom host file", res);
                }

                let name = DnsNameLabel::from_str(&res.name());
                let rtype = 1; // type A
                let rclass = 1; // class IN
                let rttl = 90; // could be better
                let rdata = res.address_as_bytes();
                let rdlength: u16 = 4;
                let compression = true;
                let comp_offset = 12;
                let record_type = DnsRecordType::Response;
                let start = SystemTime::now();
                let since_the_epoch = start
                    .duration_since(UNIX_EPOCH)
                    .expect("Time went backwards");
                let timestamp = since_the_epoch.as_secs();

                let record = DnsRecord::new(
                    name,
                    rtype,
                    rclass,
                    rttl,
                    rdlength,
                    rdata,
                    compression,
                    comp_offset,
                    record_type,
                    timestamp
                );

                answers.push(record);
                
            } else if let Some(res) = c.get(&key) {
                if self.verbose().await {
                    println!("Key '{}' retrieved from cache", key);
                }
                for record in res {
                    if record.ttl_expired(ttl_offset) {
                        if self.verbose().await {
                            println!("Key '{}' expired", key);
                        }
                        c.invalidate(&key);
                        expired = true;
                        answers = vec![];
                        authorities = vec![];
                        additionals = vec![];
                    } else if !expired  {
                        incache = true;
                        match record.record_type() {
                            DnsRecordType::Response => {
                                answers.push(record);
                            },
                            DnsRecordType::Authority => {
                                authorities.push(record)
                            },
                            DnsRecordType::Additional => {
                                additionals.push(record)
                            },
                        }
                    }
                }
            }

            if (expired || !incache) && !inhosts {
                let remote_dns = if let Some(r) = custom_dns_resolver {
                    r.to_string()
                } else {
                    "1.1.1.1:53".to_string()
                };
                if self.verbose().await {
                    println!("Key '{}' was not in cache or was expired, resolving with {}", key, &remote_dns);
                }

                let res = match resolve_name(q, &remote_dns).await {
                    Ok(res) => res,
                    Err(e) => return Err(e),
                };
                let msg = DnsMessage::from_bytes(res.as_ref()).or(Err(format!("[MK_ANSWER] Error parsing answer {:?}", res)).into())?;
                let adds = msg.additionals();
                if !adds.is_empty() {
                    additionals.extend_from_slice(adds);
                }
                let auths = msg.authorities();
                if !auths.is_empty() {
                    authorities.extend_from_slice(auths);
                }

                let mut to_cache = msg.responses()
                    .iter()
                    .filter(|r| (r.rttl() as u64 + ttl_offset) != 0)
                    .map(|r| r.to_owned())
                    .collect::<Vec<DnsRecord>>();
                
                to_cache.extend_from_slice(&additionals);
                to_cache.extend_from_slice(&authorities);
                if !to_cache.is_empty() {
                    c.insert(key, to_cache.to_vec());
                }
            
                answers.extend_from_slice(msg.responses());
            }
            
        }
        
        let res = DnsMessage::new(id, flags, q.questions().to_vec(), answers, authorities, additionals);
        Ok(res)
    }
}

async fn resolve_name(msg:&DnsMessage, custom_dns_resolver: &str) -> Result<Vec<u8>, Stderr> {
    /* TODO:
        - Replace with a legit resolver :D
    */
    let mut stream = tokio::net::TcpStream::connect(&custom_dns_resolver).await.or(Err(format!("[RESOLVE_NAME] Error connecting to '{}'", custom_dns_resolver)).into())?;
    
    stream.writable().await.or(Err("[RESOLVE_NAME] Error while waiting for the stream to be WRITABLE").into())?;
    let raw = msg.raw();
    let len = (raw.len() as u16).to_be_bytes();
    stream.write_all(&len).await.or(Err("[RESOLVE_NAME] Error writing len").into())?;
    stream.write_all(raw).await.or(Err("[RESOLVE_NAME] error writing raw").into())?;


    let mut buff = [0; 1024];
    stream.readable().await.or(Err("[RESOLVE_NAME] Error while waiting for the stream to be READABLE").into())?;
    let len = stream.read(&mut buff).await.or(Err("[RESOLVE_NAME] Error reading resolver response").into())?;
    Ok(buff[2..len].to_vec())
}