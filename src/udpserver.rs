use async_trait::async_trait;
use tokio::net::UdpSocket;
use mini_moka::sync::Cache;

use crate::Stderr;
use crate::server::Server;
use crate::dnsrecord::DnsRecord;
use crate::hosts_file_parser::Host;

#[derive(Debug)]
pub struct UdpServer {
    listener: UdpSocket,
    ttl_offset: u64,
    custom_dns_resolver: Option<String>,
    verbose: bool,
    hosts: Vec<Host>
}

impl UdpServer {
    pub async fn new(addr: &str, port: usize, ttl_offset: u64, custom_dns_resolver: Option<String>, verbose: bool, hosts: Option<Vec<Host>>) -> Result<Self, Stderr> {
        let listener = UdpSocket::bind(format!("{}:{}", addr, port)).await?;
        println!("UdpServer bound to: {}:{}", addr, port);

        let hosts = if let Some(h) = hosts {
            h
        } else {
            vec![]
        };
        
        Ok(Self {listener, ttl_offset, custom_dns_resolver, verbose, hosts})
    }
}

#[async_trait]
impl Server for UdpServer {
    async fn run_loop(&mut self, cache: &Cache<String, Vec<DnsRecord>>) -> Result<(), Stderr> {
        let mut buff = [0; 514];
        loop {
            
            let Ok((len, addr)) = self.listener.recv_from(&mut buff).await else {
                continue;
            };

            if self.verbose {
                println!("UDP: Received {} bytes from {}", len, addr);
            }

            let Ok(dns_query) = Self::parse(&buff[..len]).await else {
                continue;
            };

            if self.verbose {
                println!("Client {} wants:", addr);
                for q in dns_query.questions() {
                    println!(" - {}", q.to_string())
                }
            }
            
            let total = match self.mk_answer(&dns_query, cache, self.ttl_offset, &self.custom_dns_resolver.clone()).await {
                Ok(r) => r.as_bytes(false).await,
                Err(e) => {
                    println!("An error happened while creating the answer: {}", e);
                    continue;
                }
            };

            //let total = dns_response?.as_bytes(false).await;

            let Ok(_) = self.listener.send_to(&total, addr).await else {
                println!("Error while sending the answer");
                continue;
            };

        }
    }

    async fn verbose(&self) -> bool {
        self.verbose
    }

    async fn hosts(&self) -> &[Host] {
        &self.hosts
    }
}